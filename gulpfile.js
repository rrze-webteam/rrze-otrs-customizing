'use strict';

const
    {src, dest, watch, series} = require('gulp'),
    sass = require('gulp-sass'),
    cleancss = require('gulp-clean-css'),
    postcss = require('gulp-postcss'),
    autoprefixer = require('autoprefixer'),
    bump = require('gulp-bump'),
    semver = require('semver'),
    info = require('./package.json'),
    rename = require('gulp-rename'),
    touch = require('gulp-touch-cmd')
;

function css() {
    return src('./src/sass/rrze-otrs.scss', {
            sourcemaps: false
        })
	.pipe(sass())
	.pipe(postcss([autoprefixer()]))
	.pipe(cleancss())
	.pipe(rename(info.custom_otrs_cssfilename))
	.pipe(dest(info.otrs_agentpath + '/css'))
	.pipe(touch());
}

function cssdev() {
    return src('./src/sass/rrze-otrs.scss', {
            sourcemaps: true
        })
	.pipe(sass())
	.pipe(postcss([autoprefixer()]))
	.pipe(rename(info.custom_otrs_cssfilename))
	.pipe(dest(info.otrs_agentpath +'/css'))
	.pipe(touch());
}

function patchPackageVersion() {
    var newVer = semver.inc(info.version, 'patch');
    return src(['./package.json'])
        .pipe(bump({
            version: newVer
        }))
        .pipe(dest('./'))
	.pipe(touch());
};
function prereleasePackageVersion() {
    var newVer = semver.inc(info.version, 'prerelease');
    return src(['./package.json'])
        .pipe(bump({
            version: newVer
        }))
	.pipe(dest('./'))
	.pipe(touch());;
};

function logo() {
    return src(['./src/img/' + info.logofile])
	.pipe(rename('otrs-logo.png'))
	.pipe(dest(info.otrs_agentpath +'/img'))
	.pipe(touch());
}
function loginbackground() {
    return src(['./src/img/background/' + info.loginbackground])
	.pipe(rename('login-background.jpg'))
	.pipe(dest(info.otrs_agentpath +'/img'))
	.pipe(touch());
}
function js() {
    return src('./src/js/*.js')
	.pipe(dest(info.otrs_agentpath +'/js'))
	.pipe(touch());
}

function logo2iotrs() {
    return src(['./src/img/' + info.logofile])
	.pipe(rename('otrs-logo.png'))
	.pipe(dest(info.iotrs_path +'/img'))
	.pipe(touch());
}
function css2iotrs() {
    return src('./src/sass/rrze-otrs.scss', {
            sourcemaps: false
        })
	.pipe(sass())
	.pipe(postcss([autoprefixer()]))
	.pipe(cleancss())
	.pipe(rename(info.custom_otrs_cssfilename))
	.pipe(dest(info.iotrs_path + '/css'))
	.pipe(touch());
}
function background2iotrs() {
    return src(['./src/img/background/' + info.loginbackground])
	.pipe(rename('login-background.jpg'))
	.pipe(dest(info.iotrs_path +'/img'))
	.pipe(touch());
}
function copy_cifiles() {
    return src(['./src/img/fau-logo-37x16.png', './src/img/fau-logo-fff.png', './src/img/rrze-logo-37x16.png', './src/img/rrze-otrs-icon.png' ])
	.pipe(dest(info.otrs_agentpath +'/img'))
	.pipe(touch());
}
function copy_cifiles2iotrs() {
    return src(['./src/img/fau-logo-37x16.png', './src/img/fau-logo-fff.png', './src/img/rrze-logo-37x16.png', './src/img/rrze-otrs-icon.png' ])
	.pipe(dest(info.iotrs_path +'/img'))
	.pipe(touch());
}


exports.css = css;
exports.dev = series(cssdev, prereleasePackageVersion, logo, loginbackground, logo2iotrs, css2iotrs, background2iotrs, copy_cifiles, copy_cifiles2iotrs);
exports.build = series(css, patchPackageVersion, logo, loginbackground, logo2iotrs, css2iotrs, background2iotrs, copy_cifiles, copy_cifiles2iotrs);

