Customizing für OTRS



# Git

Repo: https://gitlab.rrze.fau.de/rrze-webteam/rrze-otrs-customizing

Die hier produzierten Templates dienen nur dem Customizing von OTRS v6.
Die eigentlichen Sourcen des Helios-Voting Systems befinden sich in den Git-Projekten:

* https://gitlab.rrze.fau.de/adb-otrs/opm-rrze-design
* https://gitlab.rrze.fau.de/adb-otrs/iotrs

## Gulp Builder

Der Gulp-Builder leistet folgendes:

- Erstellung der CSS-Dateien für das Produktivsystem
- Kopieren der ausgewählten Logos und Bilder an die jeweils richtige Stelle  

Aufruf:

  ./gulp build
     Zur Erstellung der stabilen, öffentlichen Version. 
     Erstellte Dateien werden in den src-default Ordner kopiert.
  
  ./gulp dev
     Zur Erstellung der ENtwickler-Version der CSS-Dateien. (Nicht minifiziert).
      Erstellte Dateien werden in den src-default Ordner kopiert.
    

Um Logo und Hintergrundbild zu tauschen, müssen die Quellbilder (welches in src/img/ liegen)
lediglich in den Attributen der package.json getauscht werden:

  "logofile": "otrs7.png",
  "loginbackground": "admission-2974645b_1920.jpg",


Die hier gewählten Dateien werden dann durch den Buildprozess automatisch an die richtige
Stelle kopiert und dabei auch umbenannt.

## Ordner Screenshots

Beispiel Screenshots wie das Customizing bei richtiger Nutzung aussehen soll.

## Ordner src

In diesem Ordner befinden sich die Quelldateien für die CSS-Dateien, Bilder etc.
Die CSS Dateien werden mittels SASS-Compiler gebaut. Hierzu wird der Gulp-Builder verwendet.
Beum Buildprozess werden die erzeugten Resourcen in den Ordner src-default kopiert.

## Ordner iotrs

Die hier bearbeiteten Dateien entsprechenden den Dateien im Git Projekt
https://gitlab.rrze.fau.de/adb-otrs/iotrs 


Diese Dateien werden unter der OTRS-Domain als statische Seiten aufgerufen.


## Ordner apache
Ebenfalls enthalten ist sind Konfigurationseinstellungen für den Apache Virtual Hosts (bzw. .htaccess), die z.B. in
der Apache-Konfigurationsdateien zu ergänzen oder über 
  Include iotrs.conf
 einzufügen sind

## Order skins

Diese Inhalte sollen mit dem Ordner
    /opt/otrs/var/httpd/htdocs/skins/
synchronisiert werden.


## Order Templates

Die hierin befindlichen Ordner/Dateien sollen in den Git-Ordner 
    Custom/Kernel/Output/HTML/Templates/
kopiert werden.

OTRS Dokumentation und Defaults siehe: https://github.com/OTRS/otrs/tree/rel-6_0_27/Kernel/Output/HTML/Templates/Standard


## Order Screenshots

Screenshots, wie die Umsetzung bei richtiger Konfiguration aussehen wird
 

